## console.py
## Author:   James Thiele
## Date:     27 April 2004
## Version:  1.0
## Location: http://www.eskimo.com/~jet/python/examples/cmd/
## Copyright (c) 2004, James Thiele

import os
import cmd
import readline

def cls():
    os.system('cls' if os.name == 'nt' else 'clear')

class Console(cmd.Cmd):

    def __init__(self):
        cls()
        self.help_introduction()
        cmd.Cmd.__init__(self)
        self.prompt = "=> "
        #self.intro  = "\nWelcome to Gen! Type 'help' for more information.\n"
        
    ## Command definitions ##
    def do_hist(self, args):
        """
        hist - Print a list of commands that have been entered
        """
        print self._hist
        
    def do_cls(self, args):
        """
        cls - Clear the screen
        """
        cls()

    def do_exit(self, args):
        """
        exit - Exits from the console
        """
        return -1

    def do_add(self, args):
        """
        add - Add a new genealogical entry
        ==================================
        
        Use 'add <type>' where <type> can be one of:
            indi
            event
            place
        """
        valid = ['indi', 'event', 'place']
        if args in valid:
            print 'You chose to add a ' + args
        else:
            print "You cannot add an entry of type '" + args + "'"
        return
    
    
    ## Help Topics ##
    def help_introduction(self):
        print """
        Welcome to Gen!
        ===============
        
        Gen is a commandline-based genealogical program that allows researchers to track
        people, events and relationships.
        
        Information is stored in JSON files within a local Git repository (if Git is
        available) and edited through this interactive commandline interface.
        
        This software is in early stages of development.
        
        Various help topics are available, just type 'help' or 'help <topic>'.
        """

    ## Command definitions to support Cmd object functionality ##
#    def do_EOF(self, args):
#        """Exit on system end of file character"""
#        return self.do_exit(args)
#
#    def do_shell(self, args):
#        """Pass command to a system shell when line begins with '!'"""
#        os.system(args)

    def do_help(self, args):
        """
        help - Get help on commands
        ===========================
        
        'help' or '?' with no arguments prints a list of commands for which help is available
        'help <command>' or '? <command>' gives help on <command>
        """
        ## The only reason to define this method is for the help text in the doc string
        cls()
        cmd.Cmd.do_help(self, args)


    ## Override methods in Cmd object ##
    def preloop(self):
        """Initialization before prompting user for commands.
           Despite the claims in the Cmd documentaion, Cmd.preloop() is not a stub.
        """
        cmd.Cmd.preloop(self)   ## sets up command completion
        self._hist    = []      ## No history yet
        self._locals  = {}      ## Initialize execution namespace for user
        self._globals = {}

    def postloop(self):
        """Take care of any unfinished business.
           Despite the claims in the Cmd documentaion, Cmd.postloop() is not a stub.
        """
        cmd.Cmd.postloop(self)   ## Clean up command completion
        print "Exiting..."

    def precmd(self, line):
        """ This method is called after the line has been input but before
            it has been interpreted. If you want to modifdy the input line
            before execution (for example, variable substitution) do it here.
        """
        self._hist += [ line.strip() ]
        return line

    def postcmd(self, stop, line):
        """If you want to stop the console, return something that evaluates to true.
           If you want to do some post command processing, do it here.
        """
        return stop

    def emptyline(self):    
        """Do nothing on empty input line"""
        pass

    def default(self, line):       
        """Called on an input line when the command is not recognized.
        """
        print "Unrecognized command. Try 'help'."

if __name__ == '__main__':
        console = Console()
        console . cmdloop() 
