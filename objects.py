__author__="Martijn van der Kleijn"
__date__ ="$Jan 6, 2014 1:04:44 PM$"

import json

class Event(object):
    id = 0
    type = ''
    description = ''
    subevent = ''

    def __init__(self, type):
        self.type = type
        
    def __repr__(self):
        return 'Event(' + self.type + ')'
    
    def to_JSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)